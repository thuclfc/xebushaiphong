$(document).ready(function () {
    //show menu
    $('.navbar-toggler').on('click',function () {
        $(this).toggleClass('active');
        $('.navbar-collapse').toggleClass('show');
    });
    $('.navbar-collapse .close').on('click',function () {
        $('.navbar-collapse').removeClass('show');
    });

    $('.btn_close_colleft').on('click',function () {
        $(this).toggleClass('active');
        $('.col-left').toggleClass('active');
    });

    // active navbar of page current
    var urlcurrent = window.location.href;
    $(".navbar-nav li a[href$='"+urlcurrent+"'],.nav-category li a[href$='"+urlcurrent+"']").addClass('active');
});
